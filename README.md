# idfive Gatsby x drupal test

This repo is a test of the gatsby-source-drupal npm package for gatsby. It is tested here with a stock contenta CMS endpoint.

- Deploys to heroku idfive-gatsby-drupal via bitbucket pipelines
- Interacts with [live-contentacms.pantheonsite.io](https://live-contentacms.pantheonsite.io/)
- Can be seen at [idfive-gatsby-drupal.herokuapp.com](https://idfive-gatsby-drupal.herokuapp.com/)
- heroku runs `gatsby build` on deploys, so make sure that can run locally.


## 🚀 Quick start

1.  **Start developing.**

    Navigate into your new site’s directory and start it up.

    ```shell
    cd idfive-gatsby-d8-starter/
    gatsby develop
    ```

1.  **Open the source code and start editing!**

    Your site is now running at `http://localhost:8000`!

    _Note: You'll also see a second link: _`http://localhost:8000/___graphql`_. This is a tool you can use to experiment with querying your data. Learn more about using this tool in the [Gatsby tutorial](https://www.gatsbyjs.org/tutorial/part-five/#introducing-graphiql)._
