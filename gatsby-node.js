/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */
const path = require(`path`)

exports.onCreateNode = ({ node }) => {
  if (node.internal.type === `recipes`) {
    console.log(node.internal.type)
  }
}
exports.createPages = async ({ graphql, actions }) => {
  // **Note:** The graphql function call returns a Promise
  // see: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise for more info
  const { createPage } = actions
  const recipes = await graphql(`
    query {
      allRecipes {
        edges {
          node {
            title
            id
          }
        }
      }
    }
  `)
  const tutorials = await graphql(`
    query {
      allNodeTutorial {
        edges {
          node {
            title
            id
          }
        }
      }
    }
  `)
  recipes.data.allRecipes.edges.forEach(({ node }) => {
    createPage({
      path: '/recipes/' + node.id,
      component: path.resolve(`./src/templates/recipe.js`),
      context: {
        id: node.id,
      },
    })
  })
  tutorials.data.allNodeTutorial.edges.forEach(({ node }) => {
    createPage({
      path: '/tutorials/' + node.id,
      component: path.resolve(`./src/templates/tutorial.js`),
      context: {
        id: node.id,
      },
    })
  })
}
