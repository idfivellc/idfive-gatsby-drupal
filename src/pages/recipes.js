import React from "react"
import { graphql } from "gatsby"
import { Link } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"


const RecipesPage = ({ data }) => {
  return (
    <Layout>
      <SEO title="Recipes" />
      <Link to="/">Home</Link>
      <h1>Recipes</h1>
      <p>This page lists recipes from contenta CMS (Basically D8)</p>
      <ul>
        {data.allRecipes.edges.map(({ node }, index) => (
          <li key={node.id}><Link to={`/recipes/${node.id}`}>{node.title}</Link></li>
        ))}
      </ul>
    </Layout>
  )
}

export const query = graphql`
  query {
    allRecipes {
      edges {
        node {
          title
          id
        }
      }
    }
  }
`

export default RecipesPage
