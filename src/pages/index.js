import React from "react"
import { Link } from "gatsby"
import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>Hi idfive Devs</h1>
    <p>Welcome to your new drupal API based Gatsby site.
      This queries against a stock <a href="https://live-contentacms.pantheonsite.io/">contenta CMS endpoint</a> and starter content.
      It could easily be modified to go against stock D8, contenta is simply slightly
      friendlier and has examples built out. Repo is on <a href="https://bitbucket.org/idfivellc/idfive-gatsby-drupal/src/master/">bitbucket.org/idfivellc/idfive-gatsby-drupal</a>.</p>
    <ul>
      <li><Link to="/recipes/">Recipes</Link></li>
      <li><Link to="/tutorials/">Tutorials</Link></li>
    </ul>
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div>
  </Layout>
)

export default IndexPage
