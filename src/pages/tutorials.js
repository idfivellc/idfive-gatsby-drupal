import React from "react"
import { graphql } from "gatsby"
import { Link } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"


const TutorialsPage = ({ data }) => {
  return (
    <Layout>
      <SEO title="Tutorials" />
      <Link to="/">Home</Link>
      <h1>Tutorials</h1>
      <p>This page lists tutorials from contenta CMS (Basically D8)</p>
      <ul>
        {data.allNodeTutorial.edges.map(({ node }, index) => (
          <li key={node.id}><Link to={`/tutorials/${node.id}`}>{node.title}</Link></li>
        ))}
      </ul>
    </Layout>
  )
}

export const query = graphql`
  query {
    allNodeTutorial {
      edges {
        node {
          title
          id
        }
      }
    }
  }
`

export default TutorialsPage
