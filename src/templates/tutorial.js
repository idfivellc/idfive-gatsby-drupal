import React from "react"
import { Link } from "gatsby"
import { graphql } from 'gatsby'
import Layout from "../components/layout"
import SEO from "../components/seo"

const TutorialPage = ({ data }) => {
  const tutorial = data.allNodeTutorial.edges[0].node;
  return (
    <Layout>
      <SEO title="Recipe: {recipe.title}" />
      <Link to="/tutorials/">back to All Tutorials</Link>
      <h1>{tutorial.title}</h1>
      <h4>Topic: {tutorial.field_topic}</h4>
      {tutorial.field_summary.value}
      <a href={tutorial.field_link.uri}>{tutorial.field_link.title}</a>
    </Layout>
  )
}

export const query = graphql`
  query($id: String!) {
    allNodeTutorial(filter: {id: {eq: $id}}) {
      edges {
        node {
          id
          title
          field_topic
          field_summary {
            value
          }
          field_link {
            title
            uri
          }
        }
      }
    }
  }
`
export default TutorialPage
