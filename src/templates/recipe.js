import React from "react"
import { Link } from "gatsby"
import { graphql } from 'gatsby'
import Layout from "../components/layout"
import SEO from "../components/seo"

const RecipePage = ({ data }) => {
  const recipe = data.allRecipes.edges[0].node;
  return (
    <Layout>
      <SEO title="Recipe: {recipe.title}" />
      <Link to="/recipes/">back to All Recipes</Link>
      <h1>{recipe.title}</h1>
      <h4>Time: {recipe.totalTime} | Difficulty: {recipe.difficulty}</h4>

      <h4>Ingredients:</h4>
      {recipe.ingredients !== null && (
        <ul>
          {recipe.ingredients.map((item, i) => (
            <li key={i}>
              {item}
            </li>
          ))}
        </ul>
      )}

      <h4>Instructions:</h4>
      {recipe.instructions}
    </Layout>
  )
}

export const query = graphql`
  query($id: String!) {
    allRecipes(filter: {id: {eq: $id}}) {
      edges {
        node {
          id
          title
          ingredients
          instructions
          totalTime
          difficulty
        }
      }
    }
  }
`
export default RecipePage
